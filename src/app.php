<?php

use Silex\Application;
use Silex\Provider\TwigServiceProvider;
use Silex\Provider\UrlGeneratorServiceProvider;
use Silex\Provider\ValidatorServiceProvider;
use Silex\Provider\ServiceControllerServiceProvider;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Encoder\MessageDigestPasswordEncoder;
use Symfony\Component\Security\Http\Event\InteractiveLoginEvent;
use Symfony\Component\Security\Http\SecurityEvents;
use Neutron\Silex\Provider\MongoDBODMServiceProvider;

$app = new Application();
$app->register(new UrlGeneratorServiceProvider());
$app->register(new ValidatorServiceProvider());
$app->register(new ServiceControllerServiceProvider());
$app->register(new TwigServiceProvider());
$app['twig'] = $app->share($app->extend('twig', function ($twig, $app) {

	return $twig;
}));

$app['user'] = $app->share(function() use ($app){
	$app['security']->getToken()->getUser();
});

$app->register(new Silex\Provider\SessionServiceProvider());

$app->register(new Silex\Provider\SecurityServiceProvider(), array(
	'security.firewalls' => [
		'main' => [
			'form' => [
				'login_path' => '/login',
				'check_path' => '/login_check',
				'default_target_path' => '/user/hello'
			],
			'logout' => array('logout_path' => '/logout', 'invalidate_session' => true),
			'pattern' => '^/(api|user|login_check|logout)',
			'users' => $app->share(function () use ($app) {
				return new App\UserProvider($app['users']);
			}),
		],

	],

));

$app['route_class'] = 'App\Route';


$app['db.example'] = $app->share(function() use ($app) {
	$m = new MongoClient($app['db.host']);
	$db = $m->selectDB("example");
	return $db;
});



return $app;
