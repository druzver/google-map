<?php

namespace App;

use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\User;
use Symfony\Component\Security\Core\Exception\UnsupportedUserException;
use Symfony\Component\Security\Core\Exception\UsernameNotFoundException;

class UserProvider implements UserProviderInterface
{

	protected $users;

	function __construct($users = [])
	{
		$this->users = $users;
	}


	public function loadUserByUsername($username)
	{
		if (!isset($this->users[$username])) {
			throw new UsernameNotFoundException(sprintf('"%s" does not exist.', $username));
		}
		return new User($username, $this->users[$username]['password'], [], true, true, true, true);
	}

	public function refreshUser(UserInterface $user)
	{
		if (!$user instanceof User) {
			throw new UnsupportedUserException(sprintf('Instances of "%s" are not supported.', get_class($user)));
		}
		return $this->loadUserByUsername($user->getUsername());
	}

	public function supportsClass($class)
	{
		return $class === 'Symfony\Component\Security\Core\User\User';
	}
}