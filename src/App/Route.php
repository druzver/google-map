<?php

namespace App;


use Silex\Route\SecurityTrait;

class Route extends \Silex\Route
{
	use SecurityTrait;
}