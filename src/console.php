<?php

use Symfony\Component\Console\Application;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Security\Core\User\User;

$console = new Application('My Silex Application', 'n/a');
$console->getDefinition()->addOption(new InputOption('--env', '-e', InputOption::VALUE_REQUIRED, 'The Environment name.', 'dev'));
$console->setDispatcher($app['dispatcher']);
$console
    ->register('generate-password')
    ->setDefinition(array(
         new InputOption('pass', null, InputOption::VALUE_REQUIRED, 'raw password'),
    ))
    ->setDescription('My command description')
    ->setCode(function (InputInterface $input, OutputInterface $output) use ($app) {
	    $pass = $input->getOption('pass');
	    echo PHP_EOL;
	    echo $app['security.encoder.digest']->encodePassword($pass, null);
        echo PHP_EOL;

    })
;

return $console;
