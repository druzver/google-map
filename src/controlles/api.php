<?php

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\HttpException;


$app->get('/api/marker', function(Request $request) use ($app) {
	$cursor = $app['db.example']->markers->find([
		'user'=>$app['user']->getUsername(),
	]);
	$items = [];
	foreach($cursor as $item) {
		$item['id'] = (string)$item['_id'];
		unset($item['_id']);
		$items[] = $item;
	}
	return $app->json($items);
});

$app->post('/api/marker', function(Request $request) use ($app) {
	$model = [
		'user'=>$app['user']->getUsername(),
		'name'=>$request->request->get('name'),
		'A' => (float) $request->request->get('A'),
		'F' => (float) $request->request->get('F'),
	];
	$state = $app['db.example']->markers->insert($model);
	if(!$state['ok']) {
		throw new HttpException('Can not save');
	}
	$model['id'] = (string)$model['_id'];
	unset($model['_id']);
	return $app->json($model);



});

$app->get('/api/marker/{id}', function($id, Request $request) use ($app) {
	$item = $app['db.example']->markers->findOne([
		'user'=>$app['user']->getUsername(),
		'_id' => new MongoId($id)
	]);
	if($item) {
		$item['id'] = (string)$item['_id'];
		unset($item['_id']);
	}


	return $app->json($item);
});

$app->put('/api/marker/{id}', function($id, Request $request) use ($app) {
	$item = $app['db.example']->markers->findAndModify(
		[
			'user'=>$app['user']->getUsername(),
			'_id' => new MongoId($id)
		],
		[
			'$set' => [
				'name' => $request->request->get('name'),
				'A' => (float) $request->request->get('A'),
				'F' => (float) $request->request->get('F'),
			]
		],
		null
	);

	$item = $app['db.example']->markers->findOne([
		'user'=>$app['user']->getUsername(),
		'_id' => new MongoId($id)
	]);
	if(!$item) throw new HttpException('Access denied');

	$item['id'] = (string)$item['_id'];
	unset($item['_id']);

	return $app->json($item);
});

$app->delete('/api/marker/{id}', function($id, Request $request) use ($app) {
	$ret = $app['db.example']->markers->remove(
		[
			'user'=>$app['user']->getUsername(),
			"_id" => new MongoId($id)
		]);
	return $app->json($ret);
});