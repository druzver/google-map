<?php

use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

$app->get('/login', function(Request $request) use ($app) {
	$token = $app['security.token_storage']->getToken();
	if($token) {
		return  new RedirectResponse('/');
	}

	return $app['twig']->render('login.html', array(
		'error'         => $app['security.last_error']($request),
		'last_username' => $app['session']->get('_security.last_username'),
	));
})->bind('login');

$app->post('/login_check', function(Request $request) use ($app) {
	return new Response();
})->bind('login_check');

$app->post('/logout', function(Request $request) use ($app) {
	return new Response();
})->bind('logout');

