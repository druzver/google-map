<?php
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

$app->get('/user/hello', function(Request $request) use ($app) {
	return $app['twig']->render('user/index.html', array());
})->bind('user_homepage');

