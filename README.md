
#Task

Ниже тестовое задание, которое мы хотели бы, чтобы Вы выполнили перед
собеседованием.

-----

Нужно создать мини-приложение, с простой регистрацией и авторизацией (back-end на любом языке на выбор, front-end - backbone.js), которое для залогиненного пользователя:

- покажет гугл-карту (https://developers.google.com/maps/documentation/javascript/3.exp/reference?hl=ru#Map)

- покажет все точки, введенные пользователем, на карте в виде маркеров (https://developers.google.com/maps/documentation/javascript/3.exp/reference?hl=ru#Marker) и в виде простого списка 

- при клике на точку в любом из её отображений, должен открыться popup над маркером (https://developers.google.com/maps/documentation/javascript/3.exp/reference?hl=ru#InfoWindow)

- popup должен включать как координаты точки в виде текста, так и данные геокодинга (адреса) для этой точки (https://developers.google.com/maps/documentation/javascript/3.exp/reference?hl=ru#Geocoder), адрес не должен быть сохранён в базе

- для удаления точки пользователь будет иметь 2 возможности: удалить со списка, нажав кнопку удаления, удалить прямо с popup, который появляется на карте.

- для добавления точки на карту будет использоваться кнопка, которая переведёт аппликацию в "режим добавления", таким образом клик на карте даст координаты для создания точки, из "режима добавления" можно выйти, не добавив точку.


Доп. требования:
- каждый пользователь видит и менеджит только свои точки
- точки хранятся в базе (mongo) 
- после логина весь функционал менеджмента точек должен быть SPA
- нужно использовать возможности роутинга, чтобы следующие события имели свой роут, на основе которого, карта принимала соответствующий событию вид:
1. выбрана точка (с показом popup)
2. режим добавления
3. по-умолчанию (т.е. не 1й и не 2й)
при нажатии f5 страничка должна восстановить соответствующий роуту вид

Необязательные требования:
- если покроете front-end тестами - будет очень большим плюсом, подтверждающим проф навыки.


# Instalation

The project require php extension 'mongodb.so'

```
	bower install --production
	composer install
```

##Run mongodb
```
	docker run -d -p 27017:27017 -p 28017:28017 --name mongo-test mongo
```

##Customer app configuration
You can set different app configuration, changing config/prod.php

```php
	$app['google.map.key'] = 'your google key';
	$app['db.host'] = 'mongodb://localhost:27017';
```



## NGIINX configuration

```
server {
    listen 80;
    server_name googlemap.dev;
    root /home/xuser/git/googleMap/web;
    index index_dev.php;

    location ~ \.php$ {
	fastcgi_pass   unix:/var/run/php5-fpm.sock;
	fastcgi_index index.php;
	include fastcgi_params;
    }

    location / {
        if (-f $request_filename) {
            expires max;
            break;
        }
        rewrite ^(.*) /index_dev.php last;
    }
}
```

