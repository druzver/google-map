requirejs ['marionette', 'app/controller/UserCabinet'], (Marionette, UserCabinetController)->

  class Application extends Marionette.Application

    regions:
      main: '#usercabinet'

    events:
      'start': 'onStart'

    initialize: ->

      @controller = new UserCabinetController

      @main.show @controller.layout

      router = new Marionette.AppRouter
        controller: @controller
        appRoutes:
          '': 'indexAction'
          '/': 'indexAction'
          'marker/add' : 'addAction'
          'marker/edit/:id' : 'editAction'
          'marker/remove/:id' : 'removeAction'
          '*index' : 'indexAction'


    onStart: ()->
      Backbone.history.start()

  (window.app = new Application()).start()





