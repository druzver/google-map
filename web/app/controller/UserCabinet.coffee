define [
  'marionette',
  'app/view/layout/UserCabinet',
  'app/view/Map',
  'app/view/MarkerList',
  'app/view/Marker',
  'app/collection/Markers'
], (Marionette, UserCabinetLayout, MapView, MarkerListView, MarkerView, MarkersCollection)->

  class UserCabinetController extends Marionette.Controller

    initialize: (options)->
      @layout = new UserCabinetLayout
        height: 500

      @markersCollection = new MarkersCollection()

    getListView: ->
      @listView = new MarkerListView
        collection: @markersCollection

    getMapView: ->
      return @mapView if @mapView
      @mapView = new MapView
        collection: @markersCollection

    goHome: ->
      Backbone.history.navigate '/',
        trigger: true

    edit: (model)->
      view = new MarkerView
        model: model

      mapView = @getMapView()
      mapView.selectEnable()

      @listenTo view, 'save', ()-> @goHome()
      @listenTo mapView, 'select', (latLng)->
        model.setPosition latLng

      @layout.sidebar.show view
      @layout.map.show mapView

    indexAction: ->
      mapView = @getMapView()
      mapView.selectDisable()

      listView = @getListView()
      @listenTo listView, 'childview:click', (view, options)->
        mapView.center options.model.getPosition()

      @markersCollection.fetch
        success: =>
          @layout.sidebar.show listView
          @layout.map.show mapView

    addAction: ->
      @edit @markersCollection.create {},{wait: true}

    editAction: (id)->
      model =  @markersCollection.create id: id
      model.fetch
        error: => @goHome()
        success: => @edit model

    removeAction: (id)->
      model =  @markersCollection.create id: id, {wait: true}

      model.fetch
        error: => @goHome()
        success: =>
          if confirm("Are you sure you want to delete marker #{model.get 'name'}? ")
            model.destroy()
          @goHome()

    onDestroy: ->
      components = [
        @layout,
        @markersCollection,
        @mapView
        @listView
      ]
      for o in components
        o.destroy() if o

