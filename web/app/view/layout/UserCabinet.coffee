define ['marionette'], (Marionette)->

  class Layout extends Marionette.LayoutView

    className: 'row'

    defaults:
      height: 500

    regions:
      sidebar: '.js-sidebar'
      map: '.js-map'


    template: _.template '
      <div class="js-sidebar col-md-4" style="height:100%"></div>
      <div class="js-map col-md-8" style="height:100%"></div>
    '

    initialize: (options)->
      @mergeOptions options, @defaults
      super


    onRender: ->
      @$el.height @options.height






