define ['marionette', ], (Marionette)->

  class Map extends Marionette.ItemView

    template: false

    defaults:
      #google key hardcode
      apiKey: GOOGLEMAPKEY || 'AIzaSyB91IGn9HiBvTO_lZV_7iUHLa1Nd2rgEsw'
      height: '100%'
      zoom: 8

    poupUpTemplate: _.template '
      <h1> <%- model.name %></h1>
      <ul> <%= adresHtml %> </ul>
      <a href="#/marker/remove/<%= model.id %>" >remove</a>
    '

    adressTemplate: _.template '
        <li> <%= adres.formatted_address %> </li>
    '

    initialize: (options)->
      @options = _.defaults options, @defaults
      @script = @loadScript @options.apiKey
      @modeSelect = no
      @_mapDefer = $.Deferred()
      @mapLoad = @_mapDefer.promise()
      @selectMarker = null

      @listenTo @options.collection, 'add', @onItemAdd
      @listenTo @options.collection, 'remove', @onItemRemove
      @listenTo @options.collection, 'change', @onItemChange
      super

    selectEnable: ->
      @modeSelect = true

    selectDisable: ->
      @modeSelect = false
      @mapLoad.then =>
        @selectMarker.setMap null

    onItemChange: (model)->
      model.marker.setPosition model.getPosition()

    onItemRemove: (model)->
      return unless model.marker
      model.marker.setMap null

    onItemAdd: (model)->
      @mapLoad.then =>
        @showMarker model

    center: (position)->
      @mapLoad.then =>
        @map.setCenter position

    loadScript: (key)->
      defer = $.Deferred()
      if google?.maps
        defer.resolve()
      else
        window.initMap = -> defer.resolve()
        script = document.createElement 'script'
        script.type = "text/javascript"
        script.src = "https://maps.googleapis.com/maps/api/js?key=#{key}&callback=initMap&v=3.20"
        document.body.appendChild script
      defer.promise()

    showMarker: (model)->
      marker = model.getMarker()
      marker.setMap @map
      marker.addListener 'click', (e)=>
        @onMarkerClick e, model.marker
      marker

    onRender: ->
      @$el.height @options.height
      @script.done => @renderMap()

    renderMap: ->
      @map = new google.maps.Map @el, {
        center: new google.maps.LatLng(-33, 151),
        zoom: @options.zoom,
        disableDefaultUI: true
      }
      @map.addListener 'click', _.bind @onMapClick, @

      @selectMarker = new google.maps.Marker {
        position: new google.maps.LatLng 0, 0
      }

      @_mapDefer.resolve()

      @collection.each (model)=>
          @showMarker model

    onMapClick: (e)->
      if @modeSelect
        @selectMarker.setMap @map
        @selectMarker.setPosition(e.latLng)
        @trigger 'select', e.latLng

    renderAdres: (data, marker)->
      adreses = data.map (item) =>
        @adressTemplate adres:item
      adreses.reverse().join ""

    onMarkerClick: (e, marker)->
      geocoder = new google.maps.Geocoder()

      geocoder.geocode {latLng: e.latLng}, (data, status)=>
        @infoWindow.close() if @infoWindow
        @infoWindow = new google.maps.InfoWindow {
          position: e.latLng
          content: @poupUpTemplate
            marker: marker
            adresHtml: @renderAdres data
            model: marker.getModel().toJSON()
        }

        @infoWindow.open @map, marker

