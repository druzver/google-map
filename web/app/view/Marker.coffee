define ['marionette',], (Marionette)->
  class MarkerView extends Marionette.ItemView

    tagName: 'form'

    ui:
      'nameField': '.js-name-field'
      'error': '.js-error'
      'errorText': '.js-error-text'
      'submit': '.js-submit'

    triggers:
      'click @ui.remove': 'remove'
      'click @ui.edit': 'edit'

    modelEvents:
      'change': 'validate render'

    events:
      'submit': 'onSubmit'

    serializeData: ->
      data = super
      data.title = if @model.isNew() then 'New marker' else 'Edit marker'
      data.A ?= 'no'
      data.F ?= 'no'
      data

    template: _.template '
      <h3><%= title %></h3>
      <div class="js-error hidden alert alert-danger">
        <p class="js-error-text"></p>
      </div>
      <div class="form-group">
        <label for="marker_mame">Name</label>
        <input type="text" value="<%= name %>" class="form-control js-name-field" id="marker_mame" placeholder="Name">
      </div>
      <p>(<%= A %>,<%= F %>)</p>

      <button class="js-submit btn btn-success">Save</button>
      <a href="#/" class="btn btn-default" >Back</a>
    '

    validate: ->
      @model.set
        'name': @ui.nameField.val()

      isValid = @model.isValid()
      @showError @model.validationError

      isValid

    onSubmit: (e)->
      e.preventDefault()
      e.stopPropagation()
      return unless @validate()
      @model.save {}, {
        success: _.bind @onSuccess, @
        error: _.bind @onError, @
      }

    onSuccess: ->
      @trigger 'save'

    showError: (message)->
      @ui.error.toggleClass 'hidden', !message
      @ui.errorText.text message

    onError: (model, error)->
      @showError error.statusText

