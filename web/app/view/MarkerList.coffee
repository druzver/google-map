define ['marionette', ], (Marionette)->

  class ItemView extends Marionette.ItemView

    tagName: 'tr'

    ui:
      'select':'.js-select'

    triggers:
      'click @ui.select': 'click'

    template: _.template '
      <td class="js-select">
          <span><%- name %></span>
      </td>
      <td width="50">
          <a href="#/marker/edit/<%= id %>" class="" title="Edit">
              <span class="glyphicon glyphicon-edit"></span>
          </a>
          <a href="#/marker/remove/<%= id %>" title="Remove">
              <span class="glyphicon glyphicon-remove"></span>
          </a>
      </td>
    '

  class View extends Marionette.CompositeView

    childView: ItemView

    childViewContainer: 'tbody'

    ui:
      'add':'.js-add'

    template: _.template '

      <a href="#/marker/add" class="js-add btn btn-primary " style="width: 100%;">
        <span class="glyphicon glyphicon-plus"></span> Add
      </a>

      <table class="table table-striped">
        <tbody></tbody>
      </table>

    '



