define ['marionette', 'app/model/Marker'], (Marionette, Marker)->

  class Collection extends Backbone.Collection

    url: '/api/marker'

    model: Marker

