define ['marionette'], (Marionette)->

  class Model extends Backbone.Model

    defaults:
      name: ''
      A: null
      F: null

    validate: (attrs)->
      return 'Please fill name field' unless (/\w+/).test attrs.name
      return 'Please select place on the map' unless attrs.A && attrs.F

    setPosition: (latLng)->
      @set {
        A: latLng.A
        F: latLng.F
      }

    getPosition: ->
      new google.maps.LatLng @get('A'), @get('F')

    getMarker: ->
      return @marker if @marker
      @marker = new google.maps.Marker {
        position: @getPosition()
        clickable: true
        modelId: @get 'id'
        getModel: ()=> @
      }
